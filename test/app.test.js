const request = require('supertest');
const app = require('../src/app');


describe('app', () => {
  it('responds with a not found message', function(done) {
    request(app)
      .get('/what-is-this-even')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404, done);
  });
});

describe('GET /countries', () => {
  it('responds with a json message', function(done) {
    request(app)
      .get('/users/countries')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, ["France", "Deutsches Reich", "Espagne"] , done);
  });
});

//Fonction n'est pas aboutie, le json retournée est vide {}
// describe('GET /pseudoByUserId/6VU2LW1zkZaFCGedcd34', () => {
//   it('responds with a json message', function(done) {
//     request(app)
//       .get('/users/pseudoByUserId/6VU2LW1zkZaFCGedcd34')
//       .set('Accept', 'application/json')
//       .expect(200, {pseudo:'jdupont'},  done);
//   });
// });
