const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");

require("dotenv").config();

const middlewares = require("../middlewares");
const api = require("../api");

const matchDatabase = express();

var admin = require("firebase-admin");
var serviceAccount = require("../../keyfile.json");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lumen-user-database.firebaseio.com"
    });
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
    }
}

const db = admin.firestore();
const usersCollection = db.collection("users");

module.exports = {
    follow: function(followedId, followerId) {

        return new Promise((resolve, reject) => {

            let data = {
                follower_id: followerId
            }

            usersCollection.doc(followedId).collection('followers').add(data).then(ref => {
                    resolve('OK')
                }
            )
        })
        .catch(err => {
            reject(err)
        })
    },
    getNbFollowers: function(userId) {

        return new Promise((resolve, reject) => {

            let userRef = db.collection("users/" + userId + "/followers")

            userRef.get()
                .then(querySnapshot => {
                    
                    if(querySnapshot.size == 0) {

                        resolve('Aucun follower')
                    }

                    resolve(JSON.stringify({"nbFollowers": querySnapshot.size}))
                })
        })
        .catch(err => {
            reject(err)
        })
    },
    getNbFollowedUsers: function(userId) {

        return new Promise((resolve, reject) => {

            usersCollection.get()
                .then(querySnapshot => {

                    if(querySnapshot.size == 0) {

                        resolve('Aucun utilisateur')
                    }

                    let nbFollowedUsers = 0
                    let index = 0

                    querySnapshot
                        .forEach(async doc => {

                            userRef = db.collection("users/" + doc.id + "/followers")

                            await userRef.get()
                                .then(querySnapshotFollower => {

                                    if(!(querySnapshotFollower.size == 0)) {

                                        querySnapshotFollower
                                            .forEach(follower => {

                                                if(userId == follower.data().follower_id) {

                                                    nbFollowedUsers++
                                                }
                                            })
                                    }
                                })
                            
                            index++

                            if(querySnapshot.size == index) {

                                resolve(JSON.stringify({"nbFollowedUsers": nbFollowedUsers}))
                            }
                        })
                })
        })
        .catch(err => {
            reject(err)
        })
    }
}