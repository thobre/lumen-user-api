const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const sha512 = require("js-sha512");
const ftp = require("basic-ftp");
const jsftp = require("jsftp");
const uuid = require("uuid");
const resizeImage = require("resize-optimize-images");
var fs = require('fs');

require("dotenv").config();

const middlewares = require("../middlewares");
const api = require("../api");

const matchDatabase = express();

var admin = require("firebase-admin");
var serviceAccount = require("../../keyfile.json");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lumen-user-database.firebaseio.com"
    });
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
    }
}

const db = admin.firestore();
const usersCollection = db.collection("users");

module.exports = {
    signup: function(email, pseudo, firstname, lastname, media, country, password) {

        return new Promise(async (resolve, reject) => {
            
            let addDataAvailable = true;
            const queryEmail = usersCollection.where("email","==", email);

            await queryEmail.get().then(querySnapShot => {

                if (querySnapShot.size > 0) {

                    addDataAvailable = false;
                    resolve("Email déjà existant.");
                }
            });

            const queryPseudo = usersCollection.where("pseudo","==", pseudo);

            await queryPseudo.get().then(querySnapShot => {

                if (querySnapShot.size > 0) {

                    addDataAvailable = false;
                    resolve("Pseudo déjà existant.");
                }
            });

            if (addDataAvailable) {

                let data = {
                    email: email,
                    pseudo: pseudo,
                    firstname: firstname,
                    lastname: lastname,
                    country: country,
                    password: sha512("Lum€n!" + password),
                    registrationDate: new Date()
                }

                usersCollection.add(data).then(ref => {
                    
                    let queryId = usersCollection.where("email", "==", email);

                    queryId.get().then(querySnapShot => {
    
                        querySnapShot.forEach(async doc => {
    
                            let mediaId = uuid();
                            let urlMedia = doc.id + "/" + mediaId + ".jpg";

                            usersCollection.doc(doc.id).set({
                                profilePhoto: urlMedia 
                            }, { merge: true})
                            .catch(err => {
                                reject(err);
                            });
    
                            await addProfilePhotoToOvh(media, urlMedia, doc.id);
                        })
                        .catch(err => {
                            reject(err);
                        });
                    })
                    .catch(err => {
                        reject(err);
                    });
    
                    resolve("OK");
                })
                .catch(err => {
                    reject(err);
                });

            }
        });
    }
}

async function addProfilePhotoToOvh(media, urlMedia, user_id) {

    const client = new ftp.Client();
    client.ftp.verbose = true;

    let path = "kroupe/img/" + user_id;

    console.log(path);

    try {
        await client.access({
            host: "ftp.cluster026.hosting.ovh.net",
            user: "vierovhull",
            password: "44Kroupe44"
        });

        await client.ensureDir(path);
    }
    catch(err) {
        console.log(err);
    }

    client.close();

    const Ftp = new jsftp({
        host: "ftp.cluster026.hosting.ovh.net",
        user: "vierovhull",
        pass: "44Kroupe44"
    });

    const options = {
        images: [media.path, media.path],
        width: 114,
        height: 114
    };

    await resizeImage(options);

    await fs.readFile(media.path, function read(err, data) {

        if (err) {
            console.log(err);
        }

        let pathMedia = "kroupe/img/" + urlMedia;

        console.log(pathMedia);

        Ftp.put(data, pathMedia, err => {
            if (err) {
                console.log(err)
            }
        });
    });
}