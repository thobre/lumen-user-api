const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");

require("dotenv").config();

const middlewares = require("../middlewares");
const api = require("../api");

const matchDatabase = express();

var admin = require("firebase-admin");
var serviceAccount = require("../../keyfile.json");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lumen-user-database.firebaseio.com"
    });
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
    }
}

const db = admin.firestore();

const countriesCollection = db.collection("countries");

module.exports = {
    getCountry: function() {
        let countries = [];


        return new Promise((resolve, reject) => {
            countriesCollection
                .get()
                .then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        console.log(doc.data().name);
                        countries.push(doc.data().name);
                    });
                })
                .then(() => {
                    resolve(countries);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },

    getCountryById: function(countryId) {

        let doc = countriesCollection.doc(countryId);
        let country;
        console.log("getCountry 57");

        return new Promise((resolve, reject) => {
            doc
            .get().then(doc => {
                console.log(doc.data().name);
                resolve(doc.data().name);
            })
            .catch(err => {
                reject(err);
            });
                
                // .then(function(querySnapshot) {
                //     querySnapshot.forEach(function(doc) {
                //         console.log("64");
                //         console.log(doc.data().name);
                //         country = doc.data().name;
                //     });
                // })
                // .catch(err => {
                //     reject(err);
                // });
        });
    }
};
