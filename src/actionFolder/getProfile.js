const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const ftp = require("basic-ftp");
var fs = require('fs');
const uuid = require("uuid");

require("dotenv").config();

const middlewares = require("../middlewares");
const api = require("../api");

const matchDatabase = express();

var admin = require("firebase-admin");
var serviceAccount = require("../../keyfile.json");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lumen-user-database.firebaseio.com"
    });
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
    }
}

const db = admin.firestore();

const usersCollection = db.collection("users");

module.exports = {
    getProfile: function(email) {

        let query = usersCollection.where("email", "==", email)

        return new Promise((resolve, reject) => {
            query
                .get()
                .then(function(querySnapshot) {

                    if (querySnapshot.size === 0) {

                        resolve("Email incorrect");
                    }
                    else {

                        querySnapshot.forEach(async function(doc) {

                            let imagePath = await downloadImageToOvh(doc.data().profilePhoto);
    
                            await fs.readFile(imagePath, function read(err, data) {
    
                                if (err) {
                                    console.log(err);
                                }
                                
                                resolve({
                                    country: doc.data().country,
                                    email: doc.data().email,
                                    pseudo: doc.data().pseudo,
                                    firstname: doc.data().firstname,
                                    lastname: doc.data().lastname,
                                    registrationDate: doc.data().registrationDate,
                                    profilePhoto: data.toString('base64')
                                });
                            });
                        });
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
};

async function downloadImageToOvh(imageUrl) {

    const client = new ftp.Client();
    client.ftp.verbose = true;

    try {
        await client.access({
            host: "ftp.cluster026.hosting.ovh.net",
            user: "vierovhull",
            password: "44Kroupe44"
        });

        imageUrl = "kroupe/img/" + imageUrl;
        let mediaPath = "uploads/" + uuid() + ".jpg";

        await client.downloadTo(mediaPath, imageUrl);

        return mediaPath;
    }
    catch(err) {
        console.log(err);
    }

    client.close();
}