const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const sha512 = require("js-sha512");

require("dotenv").config();

const middlewares = require("../middlewares");
const api = require("../api");

const matchDatabase = express();

var admin = require("firebase-admin");
var serviceAccount = require("../../keyfile.json");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lumen-user-database.firebaseio.com"
    });
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
    }
}

const db = admin.firestore();
const usersCollection = db.collection("users");

module.exports = {
    login: function(email, password) {

        return new Promise((resolve, reject) => {

            let query = usersCollection.where("email", "==", email);

            query.get().then(querySnapShot => {

                if (querySnapShot.size === 0) {

                    resolve({
                        email: email,
                        authentification: false,
                        description: "Email incorrect"
                    });

                } else {

                    querySnapShot.forEach(doc => {

                        let userPassword = doc.data().password;
                        
                        if (sha512("Lum€n!" + password) === userPassword) {
    
                            usersCollection.doc(doc.id).set({
                                connected: true
                            }, { merge: true })            
                            .catch(err => {
                                reject(err);
                            });
    
                            resolve({
                                email: email,
                                authentification: true
                            });
    
                        } else {
                            resolve({
                                email: email,
                                authentification: false,
                                description: "Mot de passe incorrect"
                            });
                        }
                    })
                    .catch(err => {
                        reject(err);
                    });
                }
            })
            .catch(err => {
                reject(err);
            });
        });
    },
    logoff: function(email) {

        return new Promise((resolve, reject) => {

            let query = usersCollection.where("email", "==", email);

            query.get().then(querySnapShot => {

                if (querySnapShot.size === 0) {

                    resolve({
                        email: email,
                        deconnected: false,
                        description: "Email incorrect"
                    });

                } else {

                    querySnapShot.forEach(doc => {

                        usersCollection.doc(doc.id).set({
                            connected: false
                        }, { merge: true})
                        .catch(err => {
                            reject(err);
                        });

                        resolve({
                            email: email,
                            deconnected: true
                        });
                    })
                    .catch(err => {
                        reject(err);
                    }); 
                } 
            })
            .catch(err => {
                reject(err);
            });
        });
    },
    isLog: function(email) {

        return new Promise((resolve, reject) => {

            let query = usersCollection.where("email", "==", email);

            query.get().then(querySnapShot => {

                if (querySnapShot.size === 0) {

                    resolve({
                        email: email,
                        connected: false
                    });

                } else {

                    querySnapShot.forEach(doc => {

                        let connected = doc.data().connected;

                        resolve({
                            email: email,
                            connected: connected
                        });
                    })
                    .catch(err => {
                        reject(err);
                    }); 
                }
            })
            .catch(err => {
                reject(err);
            }); 
        });
    }
}