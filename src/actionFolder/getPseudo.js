const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");

require("dotenv").config();

const middlewares = require("../middlewares");
const api = require("../api");

const matchDatabase = express();

var admin = require("firebase-admin");
var serviceAccount = require("../../keyfile.json");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lumen-user-database.firebaseio.com"
    });
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error("Firebase initialization error", err.stack);
    }
}

const db = admin.firestore();

const usersCollection = db.collection("users");

module.exports = {
    getPseudo: function(userId) {
        let countries = [];
        
        var docRef = usersCollection.doc(userId);

        return new Promise((resolve, reject) => {

            docRef.get().then(function(doc) {
                if (doc.exists) {
                    //console.log("Document data:", doc.data());
                    let pseudo = doc.data().pseudo;
                    resolve(JSON.stringify({ pseudo: pseudo}));
                } else {
                    // doc.data() will be undefined in this case
                    reject("No such document!");
                }
            }).catch(err => {
                reject(err);
            });

            // usersCollection
            //     .get()
            //     .then(function(querySnapshot) {
            //         querySnapshot.forEach(function(doc) {
            //             console.log(doc.data().name);
            //             countries.push(doc.data().name);
            //         });
            //     })
            //     .then(() => {
            //         resolve(countries);
            //     })
            //     .catch(err => {
            //         reject(err);
            //     });
        });
    }
};
