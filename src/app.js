const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const session = require ('express-session');

require("dotenv").config();

const middlewares = require("./middlewares");
const api = require("./api");
const countriesDatabase = require("./actionFolder/getCountry");
const photoDatabase = require("./actionFolder/postPhoto");
const pseudoByUserId = require("./actionFolder/getPseudo");
const profileByEmail = require("./actionFolder/getProfile");
const followFile = require('./actionFolder/follow');
const inscriptionFile = require('./actionFolder/inscription');
const connexionFile = require('./actionFolder/connexion');
const cors = require('cors');
var multer  = require('multer');

const app = express();
app.use(cors());


var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan("dev"));
app.use(helmet());

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.jpg')
    }
})
var upload = multer({ storage: storage });

app.get("/users/connexion", (req, res) => {

    const email = req.body.email;
    const password = req.body.password;

    connexionFile
        .login(email, password)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.post("/users/deconnexion", (req, res) => {

    const email = req.body.email;

    connexionFile
        .logoff(email)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.get("/users/islog", (req, res) => {

    const email = req.body.email;

    connexionFile
        .isLog(email)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.post("/users/inscription", upload.single('media'), (req, res) => {

    let media = req.file;
    const email = req.body.email;
    const pseudo = req.body.pseudo;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const country = req.body.country;
    const password = req.body.password;

    inscriptionFile
        .signup(email, pseudo, firstname, lastname, media, country, password)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});



app.get("/users/countries", (req, res) => {

    console.log("entrée 40");

    countriesDatabase
        .getCountry()
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.post("/users/profilePhoto", (req, res) => {

    let photo = req.body.photo;
    let pseudo = req.body.pseudo;

    console.log(photo);
    console.log(pseudo);

    photoDatabase
        .postPhoto(photo, pseudo)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.get("/users/pseudoByUserId/:id", (req, res) => {
    let userId = req.params.id;

    console.log(userId);

    pseudoByUserId
        .getPseudo(userId)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.get("/users/profile", (req, res) => {

    profileByEmail
        .getProfile(req.body.email)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        });
});

app.post("/users/follow", (req, res) => {

    let followedId = req.body.followedUserId
    let followerId = req.body.followerUserId

    followFile
        .follow(followedId, followerId)
        .then(result => {
            res.send(result)
        })
        .catch(err => {
            res.send(err)
        })
})

app.get("/users/followers/:idUser", (req, res) => {

    let userId = req.params.idUser

    followFile
        .getNbFollowers(userId)
        .then(result => {
            res.send(result)
        })
        .catch(err => {
            res.send(err)
        })
})

app.get("/users/followedusers/:idUser", (req, res) => {

    let userId = req.params.idUser

    followFile
        .getNbFollowedUsers(userId)
        .then(result => {
            res.send(result)
        })
        .catch(err => {
            res.send(err)
        })
})

app.use("/users/api/v1", api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
