FROM node
RUN npm install
COPY . .
EXPOSE 5002
CMD [ "node", "src/index.js" ]